package com.pragma.monolitico.posada.app.infrastructure.rest.spring.mapper;


import com.pragma.monolitico.posada.app.domain.entity.City;
import com.pragma.monolitico.posada.app.infrastructure.rest.spring.dto.CityDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface CityMapper {

    CityMapper INSTANCE = Mappers.getMapper(CityMapper.class);

    City toDomain(CityDto cityDto);

    CityDto toDto (City city);

}
