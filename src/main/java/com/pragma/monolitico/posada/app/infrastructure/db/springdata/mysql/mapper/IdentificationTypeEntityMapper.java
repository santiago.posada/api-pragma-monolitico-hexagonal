package com.pragma.monolitico.posada.app.infrastructure.db.springdata.mysql.mapper;


import com.pragma.monolitico.posada.app.domain.entity.IdentificationType;
import com.pragma.monolitico.posada.app.infrastructure.db.springdata.mysql.dbo.IdentificationTypeEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface IdentificationTypeEntityMapper {

    IdentificationTypeEntityMapper INSTANCE = Mappers.getMapper(IdentificationTypeEntityMapper.class);

    IdentificationType toDomain(IdentificationTypeEntity identificationTypeEntity);

    IdentificationTypeEntity toDbo (IdentificationType identificationType);



}
