package com.pragma.monolitico.posada.app.infrastructure.rest.spring.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IdentificationTypeDto {

    private Long id;

    private String name;

    private String description;

}
