package com.pragma.monolitico.posada.app.infrastructure.db.springdata.mysql.service.impl;

import com.pragma.monolitico.posada.app.infrastructure.db.springdata.mysql.repository.SpringDataClientRepository;
import com.pragma.monolitico.posada.app.infrastructure.db.springdata.mysql.service.CityDboService;
import com.pragma.monolitico.posada.app.domain.exceptions.city.CityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class CityDboServiceImpl  implements CityDboService {


    private final SpringDataClientRepository cityRepository;


    @Override
    public void findByIdEmpty(Long id) {
        if(this.cityRepository.findById(id).isEmpty()){
            throw new CityNotFoundException(id);
        }
    }
}
