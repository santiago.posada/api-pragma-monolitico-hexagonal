package com.pragma.monolitico.posada.app.domain.exceptions;

public class ApiRequestException extends RuntimeException{

    public ApiRequestException(String message) {

        super(message);
    }
}