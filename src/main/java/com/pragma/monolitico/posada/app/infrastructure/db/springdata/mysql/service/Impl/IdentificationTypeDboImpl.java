package com.pragma.monolitico.posada.app.infrastructure.db.springdata.mysql.service.impl;

import com.pragma.monolitico.posada.app.infrastructure.db.springdata.mysql.repository.SpringDataIdentificationTypeRepository;
import com.pragma.monolitico.posada.app.infrastructure.db.springdata.mysql.service.IdentificationTypeDboService;
import com.pragma.monolitico.posada.app.domain.exceptions.IdentificationType.IdentificationTypeNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@RequiredArgsConstructor
@Service
public class IdentificationTypeDboImpl implements IdentificationTypeDboService {

    private final SpringDataIdentificationTypeRepository identificationTypeRepository;

    @Override
    public void findByIdEmpty(Long id) {
        if(this.identificationTypeRepository.findById(id).isEmpty()){
            throw new IdentificationTypeNotFoundException(id);
        }
    }
}
