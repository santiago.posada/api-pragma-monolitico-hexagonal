package com.pragma.monolitico.posada.app.infrastructure.db.springdata.mysql.mapper;


import com.pragma.monolitico.posada.app.domain.entity.City;
import com.pragma.monolitico.posada.app.infrastructure.db.springdata.mysql.dbo.CityEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface CityEntityMapper {

    CityEntityMapper INSTANCE = Mappers.getMapper(CityEntityMapper.class);

    City toDomain(CityEntity cityEntity);

    CityEntity toDbo (City city);

}
