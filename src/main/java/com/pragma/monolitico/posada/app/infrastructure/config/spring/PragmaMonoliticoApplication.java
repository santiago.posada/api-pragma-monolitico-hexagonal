package com.pragma.monolitico.posada.app.infrastructure.config.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication(scanBasePackages="com.pragma.monolitico.posada.app.infrastructure")
@EntityScan(basePackages = "com.pragma.monolitico.posada.app.domain")
public class PragmaMonoliticoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PragmaMonoliticoApplication.class, args);
	}

}